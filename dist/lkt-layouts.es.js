import { isNumeric as t } from "lkt-tools";
import { openBlock as l, createElementBlock as o, normalizeClass as n, renderSlot as p } from "vue";
class f {
  constructor(a) {
    if (this.gap = !1, this.pad = !1, this.vPad = !1, this.hPad = !1, this.space = !1, this.marginBottom = !1, this.hasGap = !1, this.hasHPad = !1, this.hasVPad = !1, this.realGap = !1, this.realHPad = !1, this.realVPad = !1, a.gap && (this.gap = a.gap), a.pad && (this.pad = a.pad), a.vPad && (this.vPad = a.vPad), a.hPad && (this.hPad = a.hPad), a.space && (this.space = a.space), a.marginBottom && (this.marginBottom = a.marginBottom), this.space) {
      this.hasGap = !0, this.hasHPad = !0, this.hasVPad = !0, this.realGap = this.space, this.realHPad = this.space, this.realVPad = this.space;
      return;
    }
    this.gap && (this.hasGap = !0, this.realGap = this.space), this.pad ? (this.hasHPad = !0, this.hasVPad = !0, this.realHPad = this.pad, this.realVPad = this.pad) : (this.vPad && (this.hasVPad = !0, this.realVPad = this.vPad), this.hPad && (this.hasHPad = !0, this.realHPad = this.hPad));
  }
  selectors() {
    let a = [];
    return this.marginBottom && (t(this.marginBottom) && this.marginBottom > 0 ? a.push("margin-bottom-" + this.marginBottom) : a.push("margin-bottom")), this.hasGap && (t(this.realGap) && this.realGap > 0 ? a.push("gap-" + this.realGap) : a.push("gap")), this.hasHPad && this.hasVPad && this.realHPad === this.realVPad ? t(this.realHPad) && this.realHPad > 0 ? a.push("pad-" + this.realHPad) : a.push("pad") : (this.hasHPad && (t(this.realHPad) && this.realHPad > 0 ? a.push("h-pad-" + this.realHPad) : a.push("h-pad")), this.hasVPad && (t(this.realVPad) && this.realVPad > 0 ? a.push("v-pad-" + this.realVPad) : a.push("v-pad"))), a;
  }
}
const c = {
  name: "LktGrid",
  props: {
    cols: { type: [String, Number], default: 1 },
    gap: { type: [Boolean, Number], default: !1 },
    pad: { type: [Boolean, Number], default: !1 },
    vPad: { type: [Boolean, Number], default: !1 },
    hPad: { type: [Boolean, Number], default: !1 },
    space: { type: [Boolean, Number], default: !1 },
    marginBottom: { type: [Boolean, Number], default: !1 }
  },
  computed: {
    selectors() {
      let s = new f({
        gap: this.gap,
        pad: this.pad,
        vPad: this.vPad,
        hPad: this.hPad,
        space: this.space,
        marginBottom: this.marginBottom
      }).selectors();
      return s.push("grid-" + this.cols), s.join(" ");
    }
  }
}, d = (s, a) => {
  const e = s.__vccOpts || s;
  for (const [i, h] of a)
    e[i] = h;
  return e;
};
function u(s, a, e, i, h, r) {
  return l(), o("div", {
    class: n(r.selectors),
    "data-lkt": "grid"
  }, [
    p(s.$slots, "default")
  ], 2);
}
const P = /* @__PURE__ */ d(c, [["render", u]]), m = {
  name: "LktFlexRow",
  props: {
    vertical: { type: String, default: "center" },
    horizontal: { type: String, default: "flex-start" }
  }
}, g = { "data-lkt": "flex-row" };
function B(s, a, e, i, h, r) {
  return l(), o("div", g, [
    p(s.$slots, "default")
  ]);
}
const H = /* @__PURE__ */ d(m, [["render", B]]), V = {
  install: (s, a) => {
    s.component("lkt-grid", P).component("lkt-flex-row", H);
  }
};
export {
  V as default
};
