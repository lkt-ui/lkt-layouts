declare const _default: {
    name: string;
    props: {
        vertical: {
            type: StringConstructor;
            default: string;
        };
        horizontal: {
            type: StringConstructor;
            default: string;
        };
    };
};
export default _default;
