declare const _default: {
    name: string;
    props: {
        cols: {
            type: (StringConstructor | NumberConstructor)[];
            default: number;
        };
        gap: {
            type: (BooleanConstructor | NumberConstructor)[];
            default: boolean;
        };
        pad: {
            type: (BooleanConstructor | NumberConstructor)[];
            default: boolean;
        };
        vPad: {
            type: (BooleanConstructor | NumberConstructor)[];
            default: boolean;
        };
        hPad: {
            type: (BooleanConstructor | NumberConstructor)[];
            default: boolean;
        };
        space: {
            type: (BooleanConstructor | NumberConstructor)[];
            default: boolean;
        };
        marginBottom: {
            type: (BooleanConstructor | NumberConstructor)[];
            default: boolean;
        };
    };
    computed: {
        selectors(): string;
    };
};
export default _default;
