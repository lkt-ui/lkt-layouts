export interface IBoxSpacing {
    gap?: boolean | number;
    pad?: boolean | number;
    vPad?: boolean | number;
    hPad?: boolean | number;
    space?: boolean | number;
    marginBottom?: boolean | number;
}
