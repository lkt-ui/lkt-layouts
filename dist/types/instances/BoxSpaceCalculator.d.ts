import { IBoxSpacing } from "../interfaces/IBoxSpacing";
export declare class BoxSpaceCalculator implements IBoxSpacing {
    gap?: boolean | number;
    pad?: boolean | number;
    vPad?: boolean | number;
    hPad?: boolean | number;
    space?: boolean | number;
    marginBottom?: boolean | number;
    hasGap: boolean;
    hasHPad: boolean;
    hasVPad: boolean;
    realGap: boolean | number;
    realHPad: boolean | number;
    realVPad: boolean | number;
    constructor(data: IBoxSpacing);
    selectors(): string[];
}
