import {IBoxSpacing} from "../interfaces/IBoxSpacing";
import {isNumeric} from "lkt-tools";

export class BoxSpaceCalculator implements IBoxSpacing {
    gap?: boolean|number = false
    pad?: boolean|number = false
    vPad?: boolean|number = false
    hPad?: boolean|number = false
    space?: boolean|number = false
    marginBottom?: boolean|number = false

    hasGap: boolean = false
    hasHPad: boolean = false
    hasVPad: boolean = false

    realGap: boolean|number = false
    realHPad: boolean|number = false
    realVPad: boolean|number = false

    constructor(data: IBoxSpacing) {
        if (!!data.gap) this.gap = data.gap;
        if (!!data.pad) this.pad = data.pad;
        if (!!data.vPad) this.vPad = data.vPad;
        if (!!data.hPad) this.hPad = data.hPad;
        if (!!data.space) this.space = data.space;
        if (!!data.marginBottom) this.marginBottom = data.marginBottom;

        if (this.space) {
            this.hasGap = true;
            this.hasHPad = true;
            this.hasVPad = true;
            this.realGap = this.space;
            this.realHPad = this.space;
            this.realVPad = this.space;
            return;
        }

        if (this.gap) {
            this.hasGap = true;
            this.realGap = this.space;
        }

        if (this.pad) {
            this.hasHPad = true;
            this.hasVPad = true;
            this.realHPad = this.pad;
            this.realVPad = this.pad;
        } else {
            if (this.vPad) {
                this.hasVPad = true;
                this.realVPad = this.vPad;
            }
            if (this.hPad) {
                this.hasHPad = true;
                this.realHPad = this.hPad;
            }
        }
    }

    selectors() {
        let r = [];

        if (this.marginBottom) {
            if (isNumeric(this.marginBottom) && this.marginBottom > 0) {
                r.push('margin-bottom-'+this.marginBottom);
            } else {
                r.push('margin-bottom');
            }
        }

        if (this.hasGap) {
            if (isNumeric(this.realGap) && this.realGap > 0) {
                r.push('gap-'+this.realGap);
            } else {
                r.push('gap');
            }
        }

        if (this.hasHPad && this.hasVPad && this.realHPad === this.realVPad) {
            if (isNumeric(this.realHPad) && this.realHPad > 0) {
                r.push('pad-'+this.realHPad);
            } else {
                r.push('pad');
            }
        } else {
            if (this.hasHPad) {
                if (isNumeric(this.realHPad) && this.realHPad > 0) {
                    r.push('h-pad-'+this.realHPad);
                } else {
                    r.push('h-pad');
                }
            }
            if (this.hasVPad) {
                if (isNumeric(this.realVPad) && this.realVPad > 0) {
                    r.push('v-pad-'+this.realVPad);
                } else {
                    r.push('v-pad');
                }
            }
        }

        return r;
    }
}