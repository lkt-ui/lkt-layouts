/* eslint-disable import/prefer-default-export */
import LktGrid from "./lib-components/LktGrid.vue";
import LktFlexRow from "./lib-components/LktFlexRow.vue";
import {App} from "vue";

const LktLayouts = {
    install: (app: App, options: any) => {
        app.component('lkt-grid', LktGrid)
            .component('lkt-flex-row', LktFlexRow);
    },
};

export default LktLayouts;